# Mastodon

Allowing a bot/script/... to interact with (most of?) the Fediverse is a two step process. You first need to register your client (Tusky, MySuperTootingBot...) on an instance and then log in to an account using this client. To avoid the log in process each time, it is possible to create an access token for the bot.

This simple interactive script performs those 2 steps. It should work with any servers compatible with the Mastodon API (Mastodon, Pleroma...)

Creates credentials files in the script's directory.

Compatible with two-factor authentifaction (2FA).

Those credentials are meant to be used with [Mastodon.py](https://github.com/halcy/Mastodon.py/).

## Requirements

- [Mastodon.py](https://github.com/halcy/Mastodon.py/)

## Usage

```shell
register.py [-h] [--register-only] [--login-only FILE] [--server SERVER] username
```

`username`: name used mainly to name the credential files except when `--register-only` is used (see below) in which case it is the name that will be registered as a client (eg. 'TootingApp')

**Options:**

- `--register-only`: only register the client and skip token generation.
- `--login-only <FILE>`: skip client registeration and only generate token using the given client credentials file.
- `--server <SERVER>`: Server name (without URL scheme, eg. 'mastodon.social'). If omitted; the script will prompt for it. Useless if `--login-only` is set as the server name will be read in the given file.

Registeration only need to be done once per client and per server. Once the client credentials are generated, it can be used for multiple accounts on the same instance.

## Example

Without any options, the scripts generates 2 files. For example, registering the client `ExampleTooter` for user `JohnMastodon` on server `mastodon.example`:

```shell
$ python3 register.py JohnMastodon
Server name (without URL scheme, eg. 'mastodon.social'): mastodon.example
Client name (eg. 'TootingApp'): ExampleTooter
Registering client ExampleTooter on mastodon.example...
Client registered, client credentials written to /path/to/script/ExampleTooter_mastodon_example_clientcred.secret
Creating access token for JohnMastodon on mastodon.example
Do you use two-factor authentication (2FA)? (y/N)y
Please visit:
https://mastodon.example/oauth/authorize?client_id=<blob>&<parameters>
Enter the code you received: <blob code>
Access granted to ExampleTooter for @JohnMastdon@mastodon.example
Credentials written to /path/to/script/JohnMastdon_mastodon_example_usercred.secret
```

Filenames are rather long but allows the same client or user to be registered on multiple instances.

Once the access token is created, you can use it in your Python scripts:

```python
from mastodon import Mastodon

mastodon = Mastodon(access_token="/path/to/script/JohnMastdon_mastodon_example_usercred.secret")
```

And do whatever you want to do.

## License

GNU GPLv3

