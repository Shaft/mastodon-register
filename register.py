#!/usr/bin/env python3
# Coypright © John Shaft. See LICENSE for complete GNU GPLv3 text
# Or visit https://framagit.org/Shaft/mastodon-register/-/blob/main/LICENSE
"""
Simple interactive script to register bot on Mastodon/Pleroma... using Mastodon.py
Performs 2 things:
    - Register apps on servers. This has to be done only once per server and per app
    - Perform first log in order to generate an access token to an account

2FA is supported

Write all credentials in 2 files so they can be re-used.
All files are written in this script's directory
"""

import sys
import argparse
from time import sleep
from getpass import getpass
from mastodon import Mastodon

parser = argparse.ArgumentParser(
    description="Registers clients to Mastodon-API compatible servers \
    and generates tokens to accounts",
    epilog="Registeration only need to be done once per client and per server. \
    Once the client credentials are generated, it can be used for multiple \
    accounts on the same instance."
)
parser.add_argument(
    "--register-only",
    help="Register the client and skip token generation. \
    It needs to be done only once per server",
    action="store_true"
)
parser.add_argument(
    "--login-only",
    help="Skip client registeration, only generate token using \
    the given client credentials file",
    metavar="FILE",
    type=str
)
parser.add_argument(
    "--server",
    "-s",
    help="Server name (without URL scheme. eg. 'mastodon.social'). \
    If omitted, you will be prompted for it. Useless if '--login-only' is set",
    metavar="SERVER",
    type=str
)
parser.add_argument(
    "username",
    help="Username (without '@fedi.example'). When using --register-only, \
    name of the client (eg. 'TootingApp')",
    type=str
)

args = parser.parse_args()

username = args.username
cred_file = args.login_only
server = args.server

def register(name: str, base_url: str) -> None:
    """
    Register clients to a server.
    *name*: a `str`, name of the client (eg. 'TootingApp')
    *base_url*: a `str`, name of the server where the client will be registered

    Writes output to a file
    """
    print(f"Registering client {name} on {base_url}...")
    client_cred = f"{sys.path[0]}/{name}_{base_url.replace('.', '_')}_clientcred.secret"
    Mastodon.create_app(
        client_name=name,
        api_base_url=base_url,
        to_file=client_cred
    )
    print(f"Client registered, credentials written to {client_cred}")

def login(name: str, cred: str) -> None:
    """
    Generates an access token to an account for a registered client

    *name*: a `str`, name of the account. Used for the filename written
    *cred*: a `str`, the path to a client credentials file genereted with register()

    Writes output to a file
    """
    with open (cred, "r") as f:
        lines = f.readlines()
        instance = lines[-2].rstrip()[8:] # Removes URL scheme
        client = lines[-1].rstrip()
    print(f"Creating access token for {client} on {instance}...")
    user_cred = f"{sys.path[0]}/{name}_{instance.replace('.', '_')}_usercred.secret"
    mastodon = Mastodon(client_id=cred)
    auth_type = input("Do you use two-factor authentication (2FA)? (y/N) ")
    if auth_type in ("y", "Y", "yes", "YES"):
        oauth_url = mastodon.auth_request_url()
        print(f"Please visit:\n{oauth_url}")
        sleep(1)
        oauth_code = input("Enter the code you received: ")
        email=None
        password=None
    else:
        email = input("Your login email: ")
        password = getpass("Your password: ")
        oauth_code = None
    mastodon.log_in(
        username=email,
        password=password,
        code=oauth_code,
        to_file=user_cred
    )
    full_name = f"@{mastodon.me()['username']}@{instance}"
    print(f"Access granted to {client} for {full_name}")
    print(f"Credentials written to {user_cred}")

if __name__ == "__main__":
    try:
        # If cred_file is set, we get the server name from here
        if server is None and cred_file is None:
            server = input("Server name (without URL scheme, eg. 'mastodon.social'): ")
        if args.register_only and cred_file is not None:
            print("Error: can't only register client and only create access token")
            sys.exit(1)
        elif args.register_only:
            register(username, server)
        elif cred_file is not None:
            login(username, cred_file)
        else:
            client_name = input("Client name (eg. 'TootingApp'): ")
            register(client_name, server)
            filename = f"{sys.path[0]}/{client_name}_{server.replace('.', '_')}"
            login(username,
            f"{filename}_clientcred.secret"
            )
    except Exception as error:
        print(f"Error: {error}")
        sys.exit(1)

